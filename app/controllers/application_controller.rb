class ApplicationController < ActionController::API
  include ErrorRescuable
  include Pundit
end
