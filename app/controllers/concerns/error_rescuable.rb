module ErrorRescuable
  extend ActiveSupport::Concern

  included do
    rescue_from ApplicationError, with: :handle_api_error

    rescue_from ActionController::ParameterMissing, with: :handle_params_error
  end

  def handle_api_error(error)
    render json: error.to_json, status: error.status
  end

  def handle_params_error(error)
  handle_api_error(ApplicationError.new("Parameter #{error.param} is missing", :bad_request))
end

end