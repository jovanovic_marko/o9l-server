class TokensController < ApplicationController
  def total_supply
    req_params = {
        module: 'stats',
        action: 'tokensupply',
        contractaddress: tokens_params.require(:contractaddress),
        apikey: Rails.application.config.etherscan_api_key
    }

    tokens_amount = Rails.cache.fetch("#{req_params.except("apikey").to_json}", :expires_in => 5.minutes) do
      JSON.parse(RestClient.get(Rails.application.config.etherscan_api_url, params: req_params))
    end

    render json: tokens_amount, status: :ok
  end

  private

  def tokens_params
    params.permit(:contractaddress)
  end
end
