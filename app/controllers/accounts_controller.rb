class AccountsController < ApplicationController

  def normal_transactions
    req_params = {
        module: 'account',
        action: 'txlist',
        address: transaction_params.require(:address),
        startblock: transaction_params[:start_block],
        sort: 'asc',
        apikey: Rails.application.config.etherscan_api_key,
    }

    transactions = Rails.cache.fetch("#{req_params.except("apikey").to_json}", :expires_in => 5.minutes) do
      JSON.parse(RestClient.get(Rails.application.config.etherscan_api_url, params: req_params))
    end

    render json: transactions, status: :ok
  end

  private

  def transaction_params
    params.permit(:address, :start_block)
  end
end
