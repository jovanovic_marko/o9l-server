require 'rails_helper'

RSpec.describe AccountsController, type: :request do
  resource 'Accounts' do
    get '/tokens/total_supply' do
      header "Content-Type", "application/json"

      context 'Successful' do
          it "returns total amount of tokens on address" do
            mocked_response = {
                "status": "1",
                "message": "OK",
                "result": Faker::Number.number(digits: 10)
            }

            request_params = {
                contractaddress: Faker::String.random(length: 12)
            }

            transaction_params = {
                params: {
                    module: 'stats',
                    action: 'tokensupply',
                    contractaddress: request_params[:contractaddress],
                    apikey: Rails.application.config.etherscan_api_key
                }
            }

            allow(RestClient)
                .to receive(:get).with('https://api.etherscan.io/api', transaction_params)
                        .and_return(mocked_response.to_json)

            do_request(request_params)

            expect(status).to eq 200
            expect(response_body).to eq mocked_response.to_json
          end
      end

      context 'Unsuccessful' do
        it "returns error when parameter is missing" do

          do_request

          expect(status).to eq 400
        end
      end
    end
  end
end