require 'rails_helper'

RSpec.describe AccountsController, type: :request do
  resource 'Accounts' do
    get '/accounts/normal_transactions' do
      header "Content-Type", "application/json"

      context 'Successful' do
          it "returns a list of normal transactions" do
            mocked_response = {
                "status": "1",
                "message": "OK",
                "result": [
                    {
                        "blockNumber": Faker::Number.number(digits: 1),
                        "timeStamp": Faker::Number.number(digits: 8),
                        "hash": Faker::String.random(length: 12),
                        "nonce": Faker::Number.number(digits: 1),
                        "blockHash": Faker::String.random(length: 12),
                        "transactionIndex": Faker::Number.number(digits: 1),
                        "from": Faker::String.random(length: 12),
                        "to": Faker::String.random(length: 12),
                        "value": Faker::Number.number(digits: 10),
                        "gas": Faker::Number.number(digits: 3),
                        "gasPrice": Faker::Number.number(digits: 3),
                        "isError": Faker::Number.number(digits: 3),
                        "txreceipt_status": Faker::Number.number(digits: 3),
                        "input": Faker::String.random(length: 12),
                        "contractAddress": Faker::String.random(length: 12),
                        "cumulativeGasUsed": Faker::Number.number(digits: 3),
                        "gasUsed": Faker::Number.number(digits: 3),
                        "confirmations": Faker::Number.number(digits: 3)
                    }
                ]
            }

            request_params = {
                address: Faker::String.random(length: 12),
                start_block: 0
            }

            transaction_params = {
                params: {
                    module: 'account',
                    action: 'txlist',
                    address: request_params[:address],
                    startblock: request_params[:startblock],
                    sort: 'asc',
                    apikey: Rails.application.config.etherscan_api_key
                }
            }

            allow(RestClient)
                .to receive(:get).with('https://api.etherscan.io/api', transaction_params)
                        .and_return(mocked_response.to_json)

            do_request(request_params)

            expect(status).to eq 200
            expect(response_body).to eq mocked_response.to_json
          end
      end

      context 'Unsuccessful' do
        it "returns error when parameter is missing" do
          request_params = {
              start_block: 0
          }

          do_request(request_params)

          expect(status).to eq 400
        end
      end
    end
  end
end