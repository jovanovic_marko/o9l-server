Rails.application.routes.draw do
  resources :accounts do
    get :normal_transactions, on: :collection
  end


  resources :tokens do
    get :total_supply, on: :collection
  end
end
